public class CustomerQueueTests
{
    [Fact]
    public void Enqueue_AddsCustomerToQueue()
    {
        // Arrange
        var queue = new CustomerQueue(5);
        var expectedQueueCount = 1;
        var customer = new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 };

        // Act
        queue.Enqueue(customer);

        // Assert
        Assert.Equal(expectedQueueCount, queue.Count);
    }

    [Fact]
    public void Enqueue_AddsCustomerToQueueMultipleItem()
    {
        // Arrange
        var queue = new CustomerQueue(5);
        var expectedQueueCount = 3;
        var customer1 = new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 };
        var customer2 = new Customer { Name = "Jane", WithdrawalAmount = 150, OrderNumber = 2 };
        var customer3 = new Customer { Name = "Bob", WithdrawalAmount = 80, OrderNumber = 3 };

        // Act
        queue.Enqueue(customer1);
        queue.Enqueue(customer2);
        queue.Enqueue(customer3);

        // Assert
        Assert.Equal(expectedQueueCount, queue.Count);
    }

    [Fact]
    public void Enqueue_ThrowsExceptionWhenQueueIsFull()
    {
        // Arrange
        var queue = new CustomerQueue(2);
        var customer1 = new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 };
        var customer2 = new Customer { Name = "Jane", WithdrawalAmount = 150, OrderNumber = 2 };
        var customer3 = new Customer { Name = "Bob", WithdrawalAmount = 80, OrderNumber = 3 };

        queue.Enqueue(customer1);
        queue.Enqueue(customer2);

        // Act & Assert
        Assert.Throws<InvalidOperationException>(() =>
        {
            queue.Enqueue(customer3);
        });
    }


    [Fact]
    public void Dequeue_RemovesCustomerFromQueue()
    {
        // Arrange
        var queue = new CustomerQueue(5);
        var expectedQueueCount = 0;
        var customer = new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 };
        queue.Enqueue(customer);

        // Act
        var actualDequeuedCustomer = queue.Dequeue();

        // Assert
        Assert.Equal(expectedQueueCount, queue.Count);
        Assert.Equal(customer, actualDequeuedCustomer);
    }


    [Fact]
    public void Dequeue_RemovesCustomerFromQueueMultipleTimes()
    {
        // Arrange
        var queue = new CustomerQueue(5);
        var expectedQueueCount = 0;
        var customer1 = new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 };
        var customer2 = new Customer { Name = "Jane", WithdrawalAmount = 150, OrderNumber = 2 };
        var customer3 = new Customer { Name = "Bob", WithdrawalAmount = 80, OrderNumber = 3 };
        queue.Enqueue(customer1);
        queue.Enqueue(customer2);
        queue.Enqueue(customer3);

        // Act
        var dequeuedCustomer1 = queue.Dequeue();
        var dequeuedCustomer2 = queue.Dequeue();
        var dequeuedCustomer3 = queue.Dequeue();

        // Assert
        Assert.Equal(expectedQueueCount, queue.Count);
        Assert.Equal(customer1, dequeuedCustomer1);
        Assert.Equal(customer2, dequeuedCustomer2);
        Assert.Equal(customer3, dequeuedCustomer3);
    }

    [Fact]
    public void Dequeue_ThrowsExceptionWhenQueueIsEmpty()
    {
        // Arrange
        var queue = new CustomerQueue(2);
        var customer1 = new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 };
        var customer2 = new Customer { Name = "Jane", WithdrawalAmount = 150, OrderNumber = 2 };

        queue.Enqueue(customer1);
        queue.Enqueue(customer2);
        queue.Dequeue();

        // Act & Assert
        Assert.Throws<InvalidOperationException>(() =>
        {
            queue.Dequeue();
        });
    }

    [Fact]
    public void Peek_ReturnsCustomerWithoutRemovingFromQueue()
    {
        // Arrange
        var queue = new CustomerQueue(5);
        var customer = new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 };
        queue.Enqueue(customer);

        // Act
        var actualPeekedCustomer = queue.Peek();

        // Assert
        Assert.Equal(1, queue.Count);
        Assert.Equal(customer, actualPeekedCustomer);
    }

    [Fact]
    public void Clear_RemovesAllCustomersFromQueue()
    {
        // Arrange
        var queue = new CustomerQueue(5);
        var expectedQueueCount = 0;
        queue.Enqueue(new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 });

        // Act
        queue.Clear();

        // Assert
        Assert.Equal(expectedQueueCount, queue.Count);
    }

    [Fact]
    public void Get3NextCustomer_Returns3NextCustomers()
    {
        // Arrange
        var queue = new CustomerQueue(5);
        var expectedQueueCount = 3;
        var customer1 = new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 };
        var customer2 = new Customer { Name = "Jane", WithdrawalAmount = 150, OrderNumber = 2 };
        var customer3 = new Customer { Name = "Bob", WithdrawalAmount = 80, OrderNumber = 3 };
        queue.Enqueue(customer1);
        queue.Enqueue(customer2);
        queue.Enqueue(customer3);

        // Act
        var nextCustomers = queue.Get3NextCustomer();

        // Assert
        Assert.Equal(expectedQueueCount, queue.Count);
        Assert.Equal(expectedQueueCount, nextCustomers.Length);
        Assert.Equal(customer1, nextCustomers[0]);
        Assert.Equal(customer2, nextCustomers[1]);
        Assert.Equal(customer3, nextCustomers[2]);
    }

    [Fact]
    public void Get3NextCustomer_ReturnsAvailableCustomers()
    {
        // Arrange
        var queue = new CustomerQueue(5);
        var expectedQueueCount = 2;
        var customer1 = new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 };
        var customer2 = new Customer { Name = "Jane", WithdrawalAmount = 150, OrderNumber = 2 };
        var customer3 = new Customer { Name = "Bob", WithdrawalAmount = 80, OrderNumber = 3 };
        queue.Enqueue(customer1);
        queue.Enqueue(customer2);
        queue.Enqueue(customer3);
        queue.Dequeue(); // Remove one customer from the front of the queue.

        // Act
        var nextCustomers = queue.Get3NextCustomer();

        // Assert
        Assert.Equal(expectedQueueCount, queue.Count);
        Assert.Equal(expectedQueueCount, nextCustomers.Length);
        Assert.Equal(customer2, nextCustomers[0]);
        Assert.Equal(customer3, nextCustomers[1]);
    }

    [Fact]
    public void Get3NextCustomer_ReturnsLessThan3IfNotEnoughCustomers()
    {
        // Arrange
        var queue = new CustomerQueue(5);
        var expectedQueueCount = 1;
        var customer1 = new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 };
        queue.Enqueue(customer1);

        // Act
        var nextCustomers = queue.Get3NextCustomer();

        // Assert
        Assert.Equal(expectedQueueCount, queue.Count);
        Assert.Equal(expectedQueueCount, nextCustomers.Length);
        Assert.Equal(customer1, nextCustomers[0]);
    }



    [Fact]
    public void Get3NextCustomer_ReturnsEmptyArrayCustomersWhenQueueEmpty()
    {
        // Arrange
        var queue = new CustomerQueue(5);
        var expectedQueueCount = 0;

        // Act
        var nextCustomers = queue.Get3NextCustomer();

        // Assert
        Assert.Equal(expectedQueueCount, queue.Count);
        Assert.Equal(expectedQueueCount, nextCustomers.Length);
    }

    [Fact]
    public void SumOfMoneyInQueue_SumOfMoneyInQueue()
    {
        // Arrange
        var queue = new CustomerQueue(5);
        var expectedAmount = 4000;
        var customer1 = new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 };
        var customer2 = new Customer { Name = "Jane", WithdrawalAmount = 150, OrderNumber = 2 };
        var customer3 = new Customer { Name = "Bob", WithdrawalAmount = 150, OrderNumber = 3 };
        queue.Enqueue(customer1);
        queue.Enqueue(customer2);
        queue.Enqueue(customer3);

        // Act
        var totalAmountInQueue = queue.SumOfMoneyInQueue();

        // Assert
        Assert.Equal(expectedAmount, totalAmountInQueue);
    }

    [Fact]
    public void SumOfMoneyInQueue_SumOfMoneyInQueueShould0WhenNoCustomerInQueue()
    {
        // Arrange
        var queue = new CustomerQueue(5);
        var expectedAmount = 0;
        var customer1 = new Customer { Name = "John", WithdrawalAmount = 100, OrderNumber = 1 };
        var customer2 = new Customer { Name = "Jane", WithdrawalAmount = 150, OrderNumber = 2 };
        var customer3 = new Customer { Name = "Bob", WithdrawalAmount = 150, OrderNumber = 3 };
        queue.Enqueue(customer1);
        queue.Enqueue(customer2);
        queue.Enqueue(customer3);
        queue.Dequeue();
        queue.Dequeue();
        queue.Dequeue();

        // Act
        var totalAmountInQueue = queue.SumOfMoneyInQueue();

        // Assert
        Assert.Equal(expectedAmount, totalAmountInQueue);
    }
}
